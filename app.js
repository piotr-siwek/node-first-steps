console.log('starting app')

const fs = require('fs')
const _ = require('lodash')
const yargs = require('yargs')

const notes = require('./notes.js')

let argv = yargs.argv
let command = argv._[0]
console.log('command: ', command)
console.log('argv: ', argv)

if (command === 'add') {
    const note = notes.addNote(argv.title, argv.body)
    note ? console.log('note added with success') : console.log('title is duplicated, change title!')}
else if (command === 'list')
    notes.getAll()
else if (command === 'read')
    notes.readNote(argv.title, argv.body)
else if (command === 'remove')
    notes.removeNote(argv.title, argv.body)
else
    console.log('Command not recognized')
