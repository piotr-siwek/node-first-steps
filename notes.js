console.log('Starting notes.js')
const fs = require('fs')

const fetchNotes = () => {
    try {
        const notesString = fs.readFileSync('notes-data.json')
        return JSON.parse(notesString)
    } catch (error) {
        return []
    }
}

const saveNotes = notes => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes))
}

const addNote = (title, body) => {
    notes = fetchNotes()
    note = {
        title,
        body
    }


    let duplicateNotes = notes.filter(note => note.title === title)

    if (duplicateNotes.length === 0) {
        notes.push(note)
        saveNotes(notes)
        return note
    }
}

const getAll = () =>
    console.log('Getting all notes')

const readNote = title => {
    console.log('Reading note', title)
}

const removeNote = title => {
    let notes = fetchNotes()
    let removed = notes.filter(note => note.title !== title)
    saveNotes(removed)

    notes.length === removed.length ? console.log('no notes was removed') : console.log(`Your removed this note - title: ${title}`)
}
module.exports = {
    addNote,
    getAll,
    readNote,
    removeNote
}
